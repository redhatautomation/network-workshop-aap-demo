# network-workshop-aap-demo

## Getting started
Locate your evironment and workshop credentials from the workshop URL. For example https://wblcm.example.opentlc.com/


### Workbench Information (example)
Student1 (please note unique info is assigned for each workshop)

VS Code access
To login to Visual Studio Code via your web browser please go here:
WebUI link:	https://student1.wblcm.example.opentlc.com/editor
password:	jpx5r1dl

Automation controller
To login to the Automation controller WebUI use the following credentials:
WebUI link:	https://student1.wblcm.example.opentlc.com
username:	admin
password:	jpx5r1dl

## VS Code
The following steps are completed using the workshop's web based VS code terminal. 
Clone the following Gitlab repository to the home directory of the VS Code

```
git clone https://gitlab.com/redhatautomation/network-workshop-aap-demo.git
cd network-workshop-aap-demo.git
```
You must edit/save your unique credentials and controller information into vars.yml

```
vim vars.yml
```

```
host: https://#Change
username: admin
password: #Change
```

Automate the AAP controller lab as Code by runnning the following playbook from Ansible-Navigator.

```
ansible-navigator run controller.yml -m stdout
```

## Demo
This is the suggested Demo items to review with the attendees
### AAP Overview
* dashboard
* inventory 
* project
### Job-Template
Run and explain the job template "Backup network configurations"
### Workflow
Run and explain the workflow "Workshop Workflow"
* Explain the survey for the banner
* The job templates "network-banner and network-user should succeed. if not, the "Network Automation - Restore" will roll the configuration back.
* If the workflow succeeded then manually run the job template "Network Automation - Restore" afterwards to demonstrate how it would have worked during a failure.


 
 
 
 